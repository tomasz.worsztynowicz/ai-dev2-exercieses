import { execute } from './core';
import { functions } from './exercises/functions';

await execute(functions);
