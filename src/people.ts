import { match } from 'fp-ts/Either';
import { pipe } from 'fp-ts/function';
import { execute } from './core';
import { buildIndex, people } from './exercises/people';

const createIndex = async () =>
  pipe(
    await buildIndex()(),
    match(
      err => console.error(err),
      result => console.log(result),
    ),
  );

await execute(people);
