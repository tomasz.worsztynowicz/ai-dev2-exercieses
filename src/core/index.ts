import { match } from 'fp-ts/Either';
import { pipe } from 'fp-ts/function';
import { TaskEither } from 'fp-ts/TaskEither';
import { dryRunExercise, getExerciseDef, runExercise } from '../infrustructure/aidev2client';
import { Token } from '../infrustructure/aidev2client/types.ts';

export type Action<ExerciseDef extends BaseExerciseDef, ExerciseResult> = (
  def: ExerciseDef,
) => TaskEither<Error, ExerciseResult>;

export type Exercise<ExerciseDef extends BaseExerciseDef, ExerciseResult> = {
  name: string;
  action: Action<ExerciseDef, ExerciseResult>;
  getExerciseDefinitionModifier?: (token: string) => TaskEither<Error, ExerciseDef>;
  sendExerciseResultModifier?: (
    token: Token,
  ) => (r: ExerciseResult) => TaskEither<Error, ExerciseResult>;
};

export type BaseExerciseDef = {
  code: number;
};

export const execute = async <ExerciseDef extends BaseExerciseDef, ExerciseResult>(
  exercise: Exercise<ExerciseDef, ExerciseResult>,
): Promise<void> =>
  pipe(
    await runExercise<ExerciseDef, ExerciseResult>(exercise)(),
    match(
      e => console.error('fail', e),
      () => console.info('success'),
    ),
  );

export const dryRun = async <ExerciseDef extends BaseExerciseDef, ExerciseResult>(
  exercise: Exercise<ExerciseDef, ExerciseResult>,
): Promise<void> =>
  pipe(
    await dryRunExercise<ExerciseDef, ExerciseResult>(exercise)(),
    match(
      e => console.error('fail', e),
      () => console.info('success'),
    ),
  );

export const readExerciseDef = async <ExerciseDef extends BaseExerciseDef>(
  name: string,
): Promise<ExerciseDef> =>
  pipe(
    await pipe(getExerciseDef<ExerciseDef>(name))(),
    match(
      e => {
        throw e;
      },
      t => t,
    ),
  );
