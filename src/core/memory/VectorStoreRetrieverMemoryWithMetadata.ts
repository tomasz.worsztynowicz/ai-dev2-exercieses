import { Document } from 'langchain/document';
import {
  InputValues,
  OutputValues,
  VectorStoreRetrieverMemory,
  VectorStoreRetrieverMemoryParams,
} from 'langchain/memory';

export type VectorStoreRetrieverMemoryWithMetadataParams = VectorStoreRetrieverMemoryParams & {
  metadata: Record<string, any>;
};
export class VectorStoreRetrieverMemoryWithMetadata extends VectorStoreRetrieverMemory {
  metadata: Record<string, any>;
  constructor(fields: VectorStoreRetrieverMemoryWithMetadataParams) {
    const { metadata, ...rest } = fields;
    super(rest);
    this.metadata = metadata;
  }
  async saveContext(inputValues: InputValues, outputValues: OutputValues): Promise<void> {
    const text = Object.entries(inputValues)
      .filter(([k]) => k !== this.memoryKey)
      .concat(Object.entries(outputValues))
      .map(([k, v]) => `${k}: ${v}`)
      .join('\n');
    await this.vectorStoreRetriever.addDocuments([
      new Document({ pageContent: text, metadata: this.metadata }),
    ]);
  }
}
