import { pipe } from 'fp-ts/function';
import { chain, of, TaskEither } from 'fp-ts/TaskEither';
import { Validation } from 'io-ts';
import { logPipe, tryExecute } from '../../util';
import { decode } from '../../util/decode.ts';

export type GetOptions<T> = {
  headers?: HeadersInit;
  responseValidator?: (val: unknown) => Validation<T>;
};

export type PostOptions<T> = GetOptions<T> & {
  payloadToFormData?: boolean;
};

export const post = <P, T>(
  url: string,
  payload: P,
  { payloadToFormData, headers, responseValidator }: PostOptions<T> = {},
): TaskEither<Error, T> =>
  pipe(
    tryExecute(() =>
      fetch(url, {
        method: 'POST',
        headers,
        body: payloadToFormData ? (payload as FormData) : JSON.stringify(payload),
      }),
    ),
    chain(r => tryExecute(() => r.json())),
    chain(body => (responseValidator ? decode(responseValidator)(body) : of(body as T))),
    logPipe('POST Response'),
  );

export const get = <T>(
  url: string,
  { headers, responseValidator }: GetOptions<T> = {},
): TaskEither<Error, T> =>
  pipe(
    tryExecute(() => fetch(url, { headers })),
    chain(r => tryExecute(() => r.json())),
    chain(body => (responseValidator ? decode(responseValidator)(body) : of(body as T))),
    logPipe('GET Response'),
  );
