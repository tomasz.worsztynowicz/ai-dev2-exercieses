import { pipe } from 'fp-ts/function';
import { chain, left, of, TaskEither } from 'fp-ts/TaskEither';
import { isNotNil } from 'ramda';
import { BaseExerciseDef, Exercise } from '../../core';
import { logPipe } from '../../util';
import { get, post } from '../httpClient';
import { ExerciseResultRequestPayload, Token, TokenRequestPayload, TokenResponse } from './types';

const aiDev2Url = process.env.AI_DEV2_EXERCISES_URL ?? '';

export const getToken = (taskName: string): TaskEither<Error, Token> =>
  pipe(
    post<TokenRequestPayload, TokenResponse>(
      `${aiDev2Url}/token/${taskName}`,
      getTokenReqPayload(),
    ),
    chain(({ token, code }) => (code === 0 ? of(token) : left(new Error('Bad response')))),
  );

const getTokenReqPayload = (): TokenRequestPayload => ({
  apikey: process.env.AI_DEV2_API_KEY ?? '',
});

export const getExerciseDef = <ExerciseDef extends BaseExerciseDef>(
  name: string,
): TaskEither<Error, ExerciseDef> =>
  pipe(
    getToken(name),
    chain(token => getExerciseDefByToken(token)),
  );

const getExerciseDefByToken = <ExerciseDef extends BaseExerciseDef>(
  token: Token,
): TaskEither<Error, ExerciseDef> =>
  pipe(
    get<ExerciseDef>(`${aiDev2Url}/task/${token}`),
    chain(resp => (resp.code === 0 ? of(resp) : left(new Error('Bad response')))),
    logPipe('exercise definition'),
  );

export const runExercise = <ExerciseDef extends BaseExerciseDef, ExerciseResult>(
  exercise: Exercise<ExerciseDef, ExerciseResult>,
): TaskEither<Error, ExerciseResult> =>
  pipe(
    getToken(exercise.name),
    chain(token =>
      pipe(
        isNotNil(exercise.getExerciseDefinitionModifier)
          ? exercise.getExerciseDefinitionModifier(token)
          : getExerciseDefByToken<ExerciseDef>(token),
        chain(exercise.action),
        logPipe('exercise result'),
        chain((result: ExerciseResult) =>
          isNotNil(exercise.sendExerciseResultModifier)
            ? exercise.sendExerciseResultModifier(token)(result)
            : sendExerciseResult<ExerciseResult>(token)(result),
        ),
      ),
    ),
  );

export const dryRunExercise = <ExerciseDef extends BaseExerciseDef, ExerciseResult>(
  exercise: Exercise<ExerciseDef, ExerciseResult>,
): TaskEither<Error, ExerciseResult> =>
  pipe(
    getToken(exercise.name),
    chain(token =>
      pipe(
        exercise.getExerciseDefinitionModifier
          ? exercise.getExerciseDefinitionModifier(token)
          : getExerciseDefByToken<ExerciseDef>(token),
        chain(exercise.action),
      ),
    ),
  );

const sendExerciseResult =
  <ExerciseResult>(token: Token) =>
  (r: ExerciseResult): TaskEither<Error, ExerciseResult> =>
    pipe(
      of<Error, ExerciseResultRequestPayload<ExerciseResult>>({ answer: r }),
      logPipe('result sent'),
      chain(payload => post(`${aiDev2Url}/answer/${token}`, payload)),
    );
