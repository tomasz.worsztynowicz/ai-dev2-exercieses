export type TokenRequestPayload = {
  apikey: string;
};

export type TokenResponse = {
  token: Token;
  code: number;
};

export type ExerciseResultRequestPayload<A> = {
  answer: A;
};
export type Token = string;
