import { QdrantClient } from '@qdrant/js-client-rest';
import { pipe } from 'fp-ts/function';
import { flatMap, map, of, TaskEither } from 'fp-ts/TaskEither';
import { isNotNil } from 'ramda';
import { tryExecute } from '../../util';

export type UpsertResult = {
  operation_id: number;
  status: 'acknowledged' | 'completed';
};

export type Vector<Metadata> = {
  id: string;
  vector: number[];
  payload: Metadata;
};

export type Query = {
  collectionName: string;
  vector: number[];
  limit: number;
};

export type QueryResult<Payload> = {
  id: string;
  version: number;
  score: number;
  payload: Payload;
  vector: number[];
};
export const qdrant = new QdrantClient({ url: process.env.QDRANT_URL });

const upsertCollection = (name: string): TaskEither<Error, boolean> =>
  pipe(
    tryExecute(() => qdrant.getCollections()),
    map(({ collections }) => collections.find(collection => collection.name === name)),
    flatMap(collection =>
      isNotNil(collection)
        ? of(false)
        : tryExecute(() =>
            qdrant.createCollection(name, {
              vectors: { size: 1536, distance: 'Cosine', on_disk: true },
            }),
          ),
    ),
  );

export const upsertVectors =
  <T>(collectionName: string) =>
  (vectors: readonly Vector<T>[]): TaskEither<Error, UpsertResult> =>
    pipe(
      upsertCollection(collectionName),
      flatMap(() =>
        tryExecute(() =>
          qdrant.upsert(collectionName, {
            wait: true,
            batch: {
              ids: vectors.map(vector => vector.id),
              vectors: vectors.map(vector => vector.vector),
              payloads: vectors.map(vector => vector.payload as any),
            },
          }),
        ),
      ),
    );

export const search = <R>(q: Query): TaskEither<Error, QueryResult<R>[]> =>
  pipe(
    tryExecute(() =>
      qdrant.search(q.collectionName, {
        vector: q.vector,
        limit: q.limit,
      }),
    ),
    map(results => results as QueryResult<R>[]),
  );
