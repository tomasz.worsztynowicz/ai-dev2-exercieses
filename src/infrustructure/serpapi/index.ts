import { pipe } from 'fp-ts/function';
import { flatMap, TaskEither } from 'fp-ts/TaskEither';
import * as t from 'io-ts';
import { getJson } from 'serpapi';
import { logPipe, tryExecute } from '../../util';
import { decode } from '../../util/decode.ts';

const OrganicResult = t.type({
  position: t.number,
  title: t.string,
  link: t.string,
});
export type OrganicResult = t.TypeOf<typeof OrganicResult>;
const GoogleSearchResult = t.type({
  organic_results: t.array(OrganicResult),
});
export type GoogleSearchResult = t.TypeOf<typeof GoogleSearchResult>;

export const googleSearch = (query: string): TaskEither<Error, GoogleSearchResult> =>
  pipe(
    tryExecute(() =>
      getJson({
        engine: 'google',
        q: query,
        api_key: process.env.SERP_API_KEY,
      }),
    ),
    flatMap(decode(GoogleSearchResult.decode)),
    logPipe('RAW Serpapi results'),
  );
