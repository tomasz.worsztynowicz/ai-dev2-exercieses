import express, { ErrorRequestHandler, json, Request, Response } from 'express';
import { match } from 'fp-ts/Either';
import { pipe } from 'fp-ts/function';
import { flatMap, map } from 'fp-ts/TaskEither';
import * as t from 'io-ts';
import { answerWithGoogle } from '../../exercises/google';
import { convertMd2Html } from '../../exercises/md2html';
import { answerQuestion } from '../../exercises/ownapi';
import { converse } from '../../exercises/ownapipro/withVectorStore.ts';
import { logPipe } from '../../util';
import { decode } from '../../util/decode.ts';

const port = 3000;
const errorHandler: ErrorRequestHandler = (err, req, res, next) => {
  console.error(`Error handled: ${JSON.stringify(err)}`, err);
  res.sendStatus(500);
};

const QueryRequestPayload = t.type({
  question: t.string,
});

export const ownApiHandler = async (req: Request, res: Response) =>
  pipe(
    await pipe(
      decode(QueryRequestPayload.decode)(req.body),
      map(({ question }) => question),
      flatMap(answerQuestion),
      map(reply => ({ reply })),
    )(),
    match(
      () => res.sendStatus(500),
      result => res.json(result),
    ),
  );

export const ownApiProHandler = async (req: Request<{ thread: string }>, res: Response) =>
  pipe(
    await pipe(
      decode(QueryRequestPayload.decode)(req.body),
      logPipe('Decoded payload'),
      map(({ question }) => question),
      flatMap(converse(req.params.thread)),
      logPipe('Answer'),
      map(reply => ({ reply })),
    )(),
    match(
      () => res.sendStatus(500),
      result => res.json(result),
    ),
  );

export const answerWithGoogleHandler = async (req: Request, res: Response) =>
  pipe(
    await pipe(
      decode(QueryRequestPayload.decode)(req.body),
      logPipe('Decoded payload'),
      map(({ question }) => question),
      flatMap(answerWithGoogle),
      map(reply => ({ reply })),
      logPipe('Response'),
    )(),
    match(
      err => res.sendStatus(500),
      result => res.json(result),
    ),
  );

export const md2HtmlHandler = async (req: Request, res: Response) =>
  pipe(
    await pipe(
      decode(QueryRequestPayload.decode)(req.body),
      logPipe('Decoded payload'),
      map(({ question }) => question),
      flatMap(convertMd2Html),
      map(reply => ({ reply })),
      logPipe('Response'),
    )(),
    match(
      err => res.sendStatus(500),
      result => res.json(result),
    ),
  );

const app = express();
app.use(json());
app.use(errorHandler);
app.get('/health', (req, res) => res.sendStatus(200));
app.post('/ownapi', async (req, res) => ownApiHandler(req, res));
app.post('/ownapipro/:thread', async (req, res) => ownApiProHandler(req, res));
app.post('/google', async (req, res) => answerWithGoogleHandler(req, res));
app.post('/md2html', async (req, res) => md2HtmlHandler(req, res));

app.listen(port, () => {
  console.log(`AI Dev2 OwnApi App listening on port ${port}`);
});
