import { execute } from './core';
import { ownapi } from './exercises/ownapi';

await execute(ownapi);
