import { execute } from './core';
import { knowledge } from './exercises/knowledge';

await execute(knowledge);
