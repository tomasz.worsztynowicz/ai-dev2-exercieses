import { head } from 'fp-ts/Array';
import { pipe } from 'fp-ts/function';
import { chain, fromOption, map, TaskEither } from 'fp-ts/TaskEither';
import { OpenAI } from 'openai';
import { Embedding } from 'openai/src/resources/embeddings.ts';
import { BaseExerciseDef, Exercise } from '../../core';
import { tryExecute } from '../../util';

export type EmbeddingExerciseDef = BaseExerciseDef;
export type EmbeddingExerciseResult = number[];

const openai = new OpenAI();
const generateEmbedding = (input: string): TaskEither<Error, Array<Embedding>> =>
  pipe(
    tryExecute(() =>
      openai.embeddings.create({
        model: 'text-embedding-ada-002',
        encoding_format: 'float',
        input,
      }),
    ),
    map(({ data }) => data),
  );

const action = () =>
  pipe(
    generateEmbedding('Hawaiian pizza'),
    chain(v => fromOption(() => new Error('No embedding'))(head(v))),
    map(({ embedding }) => embedding),
  );
export const embedding: Exercise<EmbeddingExerciseDef, EmbeddingExerciseResult> = {
  name: 'embedding',
  action,
};
