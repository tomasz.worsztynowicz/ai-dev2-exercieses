import { pipe } from 'fp-ts/function';
import { map, TaskEither } from 'fp-ts/TaskEither';
import * as t from 'io-ts';
import { BaseExerciseDef, Exercise } from '../../core';
import { post } from '../../infrustructure/httpClient';

export type MemeExerciseDef = { service: string; image: string; text: string } & BaseExerciseDef;
export type MemeExerciseResult = string;
const ServiceResponse = t.type({
  requestId: t.string,
  href: t.string,
});
type ServiceResponse = t.TypeOf<typeof ServiceResponse>;

const template = 'dizzy-moles-charge-fast-1717';
const apiUrl = 'https://get.renderform.io/api/v2/render';
export const meme: Exercise<MemeExerciseDef, MemeExerciseResult> = {
  name: 'meme',
  action: ({ image, text }) =>
    pipe(
      generateMeme(image, text),
      map(({ href }) => href),
    ),
};

const generateMeme = (image: string, text: string): TaskEither<Error, ServiceResponse> =>
  pipe(
    post(apiUrl, payload(image, text), {
      headers: {
        'Content-Type': 'application/json',
        'X-API-KEY': process.env.RENDER_FORM_API_KEY ?? '',
      },
      responseValidator: ServiceResponse.decode,
    }),
  );

const payload = (image: string, text: string) => ({
  template,
  data: {
    'image.src': image,
    'title.text': text,
  },
});
