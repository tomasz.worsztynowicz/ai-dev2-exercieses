import { of } from 'fp-ts/TaskEither';
import { BaseExerciseDef, Exercise } from '../../core';

export type HelloApiExerciseDef = { cookie: string } & BaseExerciseDef;
export type HelloApiExerciseResult = string;

export const helloapi: Exercise<HelloApiExerciseDef, HelloApiExerciseResult> = {
  name: 'helloapi',
  action: v => of(v.cookie),
};
