import { file } from 'bun';
import { head } from 'fp-ts/Array';
import { pipe } from 'fp-ts/function';
import { flatMap, fromOption, map, sequenceSeqArray, TaskEither } from 'fp-ts/TaskEither';
import { Document } from 'langchain/document';
import { OpenAIEmbeddings } from 'langchain/embeddings/openai';
import { v4 as uuidv4 } from 'uuid';
import { BaseExerciseDef, Exercise } from '../../core';
import {
  QueryResult,
  search as _search,
  UpsertResult,
  upsertVectors,
  Vector,
} from '../../infrustructure/qdrant';
import { logPipe, logPipeProjection, logReplace, tap, tryExecute } from '../../util';

export type SearchExerciseDef = { question: string } & BaseExerciseDef;
export type SearchExerciseResult = string;

export type ArchiveEntry = {
  title: string;
  url: string;
  info: string;
  date: string;
};

export type ArchiveEntryWithUUID = ArchiveEntry & { uuid: string };

export const FILE_PATH = import.meta.dir + '/archiwum.json';

const embeddings = new OpenAIEmbeddings({ maxConcurrency: 5, maxRetries: 5 });
const ARCHIVE_COLLECTION_NAME = 'archive';
const DOCS_LIMIT = 300;
const getArchives = (filePath: string, limit: number): TaskEither<Error, ArchiveEntry[]> =>
  pipe(
    tryExecute(() => file(filePath).json<ArchiveEntry[]>()),
    map(archives => archives.slice(0, limit)),
  );

const mapToDocument = (archive: ArchiveEntry): Document<ArchiveEntryWithUUID> => ({
  pageContent: `TITLE: ${archive.title}
${archive.info}
 `,
  metadata: { ...archive, uuid: uuidv4() },
});

const mapToVector = (
  document: Document<ArchiveEntryWithUUID>,
  index: number,
): TaskEither<Error, Vector<ArchiveEntryWithUUID>> =>
  pipe(
    tryExecute(() => embeddings.embedDocuments([document.pageContent])),
    map(([embedding]) => ({
      id: document.metadata.uuid,
      vector: embedding,
      payload: document.metadata,
    })),
    tap(() => logReplace(`Embedded ${index + 1} documents`)),
  );

export const buildIndex = (): TaskEither<Error, UpsertResult> =>
  pipe(
    getArchives(FILE_PATH, DOCS_LIMIT),
    logPipeProjection(archives => `Read ${archives.length} archive entries`),
    map(archives => archives.map(mapToDocument)),
    flatMap(documents => sequenceSeqArray(documents.map(mapToVector))),
    flatMap(upsertVectors(ARCHIVE_COLLECTION_NAME)),
    logPipe('Build Index result'),
  );

const ask = (question: string): TaskEither<Error, QueryResult<ArchiveEntryWithUUID>> =>
  pipe(
    tryExecute(() => embeddings.embedQuery(question)),
    flatMap(queryEmbeddings =>
      _search<ArchiveEntryWithUUID>({
        collectionName: ARCHIVE_COLLECTION_NAME,
        vector: queryEmbeddings,
        limit: 1,
      }),
    ),
    flatMap(result => fromOption(() => new Error('No results'))(head(result))),
    logPipe('Query result'),
  );
export const search: Exercise<SearchExerciseDef, SearchExerciseResult> = {
  name: 'search',
  action: v =>
    pipe(
      ask(v.question),
      map(result => result.payload.url),
    ),
};
