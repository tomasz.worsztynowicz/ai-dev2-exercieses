import { pipe } from 'fp-ts/function';
import { flatMap, map, TaskEither } from 'fp-ts/TaskEither';
import * as t from 'io-ts';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { ChatPromptTemplate } from 'langchain/prompts';
import { OpenAI } from 'openai';
import { BaseExerciseDef, Exercise } from '../../core';
import { logPipe, tryExecute } from '../../util';
import { decode } from '../../util/decode.ts';
import { AIFunctionCallMessage } from '../../util/functionCall.ts';

export type ToolsExerciseDef = { question: string } & BaseExerciseDef;
export type ToolsExerciseResult = TaskToolArgs;

export const tools: Exercise<ToolsExerciseDef, ToolsExerciseResult> = {
  name: 'tools',
  action: v => callTools(v.question),
};

const taskTools: OpenAI.Chat.Completions.ChatCompletionCreateParams.Function = {
  name: 'tools',
  description: `Saves a task to the todo list or adds the event to the calendar.
  Todo task list is intended for actions that need to be done but are not time related. 
  Calendar is intended for events or actions that are time or date related. The date relation can be explicit or implicit.`,
  parameters: {
    type: 'object',
    properties: {
      tool: {
        type: 'string',
        description: `The tool to use. 
          ToDo - for task or actions that are not related to the date or time, e.g buy milk, take out the trash, talk to John
          Calendar - for actions or task that are date or time related, e.g meet Jon on Saturday at 10:00, buy milk tomorrow, take out the trash on Friday`,
        enum: ['ToDo', 'Calendar'],
      },
      desc: {
        type: 'string',
        description: 'The description of the task or event',
      },
      date: {
        type: 'string',
        description:
          'The date of the event in ISO format(YYYY-MM-DD), e.g. 2021-10-20. Required only for Calendar tool for ToDo tool the date should be empty',
      },
    },
    required: ['tool', 'desc'],
  },
};

const TaskToolArgs = t.intersection([
  t.type({
    tool: t.union([t.literal('ToDo'), t.literal('Calendar')]),
    desc: t.string,
  }),
  t.partial({
    date: t.string,
  }),
]);

type TaskToolArgs = t.TypeOf<typeof TaskToolArgs>;

const llm = new ChatOpenAI({
  modelName: 'gpt-3.5-turbo-1106',
  maxTokens: 500,
  maxRetries: 5,
  temperature: 0,
}).bind({ functions: [taskTools] });

const promptTemplate = ChatPromptTemplate.fromMessages([
  [
    'system',
    `Add the following action to the ToDo list or to the Calendar. Use your generic knowledge to decide which tool to use and given context as extension of your knowledge.
      context###
      Today is {now}
      ###`,
  ],
  ['user', '{task}'],
]);

const callTools = (task: string): TaskEither<Error, TaskToolArgs> =>
  pipe(
    tryExecute(() => promptTemplate.formatPromptValue({ task, now: new Date().toISOString() })),
    flatMap(prompt => tryExecute(() => llm.invoke(prompt))),
    flatMap(decode(AIFunctionCallMessage.decode)),
    map(v => v.additional_kwargs.function_call.arguments),
    flatMap(args => tryExecute(async () => JSON.parse(args))),
    flatMap(decode(TaskToolArgs.decode)),
    logPipe('Selected tool'),
  );
