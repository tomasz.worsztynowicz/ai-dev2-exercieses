import * as A from 'fp-ts/Array';
import { pipe } from 'fp-ts/function';
import { chain, map, of, sequenceSeqArray, TaskEither } from 'fp-ts/TaskEither';
import { ConversationChain } from 'langchain/chains';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { BufferMemory } from 'langchain/memory';
import { ChatPromptTemplate, MessagesPlaceholder } from 'langchain/prompts';
import { ChainValues } from 'langchain/schema';
import { Action, BaseExerciseDef, Exercise } from '../../core';
import { tryExecute } from '../../util';
import { bloggerTemplate, systemTemplate } from './templates.ts';

const buildConversationChain = (initPrompt: ChatPromptTemplate) => {
  const prompt = ChatPromptTemplate.fromMessages([
    initPrompt,
    new MessagesPlaceholder('history'),
    ['human', '{input}'],
  ]);
  const memory = new BufferMemory({
    returnMessages: true,
    memoryKey: 'history',
  });
  const llm = new ChatOpenAI({
    maxTokens: 500,
    temperature: 0.1,
    modelName: 'gpt-3.5-turbo',
    //modelName: 'gpt-4',
    streaming: true,
  });
  const conversationChain = new ConversationChain({ llm, memory, prompt });
  return {
    call: (values: ChainValues): TaskEither<Error, ChainValues> =>
      pipe(tryExecute(() => conversationChain.call(values, { callbacks }))),
  };
};

const callbacks = [
  {
    handleLLMNewToken(token: string) {
      process.stdout.write(token);
    },
  },
];

export type BloggerExerciseDef = { blog: string[] } & BaseExerciseDef;
export type BloggerExerciseResult = string[];

const action: Action<BloggerExerciseDef, BloggerExerciseResult> = ({ blog }) =>
  pipe(
    of(buildConversationChain(ChatPromptTemplate.fromMessages([['system', systemTemplate]]))),
    chain(conversationChain =>
      sequenceSeqArray(
        pipe(
          blog,
          A.map(chapterTitle => conversationChain.call({ input: bloggerTemplate(chapterTitle) })),
        ),
      ),
    ),
    map(responses => responses.map(r => r.response)),
  );

export const blogger: Exercise<BloggerExerciseDef, BloggerExerciseResult> = {
  name: 'blogger',
  action,
};
