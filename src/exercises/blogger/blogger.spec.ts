import { describe, expect, it } from 'bun:test';
import { isRight } from 'fp-ts/Either';
import { blogger } from './index.ts';

describe('blogger', () => {
  const { action } = blogger;
  const exerciseDef = {
    code: 0,
    msg: 'please write blog post for the provided outline',
    blog: [
      'Wstęp: kilka słów na temat historii pizzy',
      'Niezbędne składniki na pizzę',
      'Robienie pizzy',
      'Pieczenie pizzy w piekarniku',
    ],
  };
  it('should write blog', async () => {
    const result = await action(exerciseDef)();
    expect(isRight(result)).toBe(true);
    console.log(result);
  });
});
