import { head } from 'fp-ts/Array';
import { pipe } from 'fp-ts/function';
import { chain, fromOption, map, sequenceSeqArray, TaskEither } from 'fp-ts/TaskEither';
import { OpenAI } from 'openai';
import { Action, BaseExerciseDef, Exercise } from '../../core';
import { logPipe, tryExecute } from '../../util';

export type ModerationExerciseDef = { input: string[] } & BaseExerciseDef;
export type ModerationExerciseResult = number[];

const openai = new OpenAI();

const moderate = (text: string): TaskEither<Error, OpenAI.Moderation> =>
  pipe(
    tryExecute(() => openai.moderations.create({ input: text })),
    logPipe('moderation result'),
    map((r: OpenAI.ModerationCreateResponse) => r.results),
    map(head),
    chain(fromOption(() => new Error('No moderation results found'))),
  );

const action: Action<ModerationExerciseDef, ModerationExerciseResult> = ({ input }) =>
  pipe(
    sequenceSeqArray(input.map(moderate)),
    map(ms => ms.map(({ flagged }) => (flagged ? 1 : 0))),
  );

export const moderation: Exercise<ModerationExerciseDef, ModerationExerciseResult> = {
  name: 'moderation',
  action,
};
