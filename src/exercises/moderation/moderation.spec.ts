import { describe, expect, it } from 'bun:test';
import { isRight } from 'fp-ts/Either';
import { dryRunExercise } from '../../infrustructure/aidev2client';
import { moderation } from './index.ts';

describe('moderation', () => {
  it('should moderate', async () => {
    const result = await dryRunExercise(moderation)();
    expect(isRight(result)).toBe(true);
    console.log(result);
  });
});
