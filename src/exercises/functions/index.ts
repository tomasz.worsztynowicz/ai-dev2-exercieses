import { of } from 'fp-ts/TaskEither';
import { BaseExerciseDef, Exercise } from '../../core';

export type FunctionsExerciseDef = { cookie: string } & BaseExerciseDef;
export type FunctionsExerciseResult = unknown;

const addUserSchema = {
  name: 'addUser',
  description: 'Add a new user',
  parameters: {
    type: 'object',
    properties: {
      name: {
        type: 'string',
        description: 'User name',
      },
      surname: {
        type: 'string',
        description: 'User surname',
      },
      year: {
        type: 'number',
        description: 'Year of born',
      },
    },
  },
};

export const functions: Exercise<FunctionsExerciseDef, FunctionsExerciseResult> = {
  name: 'functions',
  action: v => of(addUserSchema),
};
