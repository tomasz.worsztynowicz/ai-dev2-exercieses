import * as A from 'fp-ts/Array';
import * as O from 'fp-ts/Option';
import { pipe } from 'fp-ts/function';
import { flatMap, left, map, of, TaskEither } from 'fp-ts/TaskEither';
import { LLMChain } from 'langchain/chains';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { ChatPromptTemplate } from 'langchain/prompts';
import { SystemMessage } from 'langchain/schema';
import { BaseExerciseDef, Exercise } from '../../core';
import { googleSearch, GoogleSearchResult } from '../../infrustructure/serpapi';
import { logPipe, tryExecute } from '../../util';

export type GoogleExerciseDef = BaseExerciseDef;
export type GoogleExerciseResult = string;

const llm = new ChatOpenAI({
  modelName: 'gpt-3.5-turbo',
  maxTokens: 500,
  maxRetries: 5,
  temperature: 0,
});
const prompt = ChatPromptTemplate.fromMessages([
  new SystemMessage(`Your role is to convert the user question into the Google search input which will return search results with  the best match for the user question
Write only the search input text and nothing more
Use the user-input language
Question:`),
  ['user', '{question}'],
]);

const chainLLM = new LLMChain({ llm, prompt, outputKey: 'query' });
export const google: Exercise<GoogleExerciseDef, GoogleExerciseResult> = {
  name: 'google',
  action: v => of(`https://85ef-37-30-16-59.ngrok-free.app/google`),
};

export const answerWithGoogle = (question: string): TaskEither<Error, string> =>
  pipe(
    convertQuestionToQuery(question),
    flatMap(googleSearch),
    flatMap(getLink),
    logPipe('Selected result'),
  );

const getLink = (result: GoogleSearchResult): TaskEither<Error, string> =>
  pipe(
    A.head(result.organic_results),
    O.map(({ link }) => of(link)),
    O.getOrElse(() => left(new Error('No results found'))),
  );

const convertQuestionToQuery = (question: string): TaskEither<Error, string> =>
  pipe(
    tryExecute(() => chainLLM.call({ question })),
    map(({ query }) => query),
    logPipe('Google query'),
  );
