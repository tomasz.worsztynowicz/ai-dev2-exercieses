import * as A from 'fp-ts/Array';
import { pipe } from 'fp-ts/function';
import { flatMap, map, sequenceArray, TaskEither } from 'fp-ts/TaskEither';
import * as t from 'io-ts';
import { LLMChain } from 'langchain/chains';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { ChatPromptTemplate } from 'langchain/prompts';
import { SystemMessage } from 'langchain/schema';
import { BaseExerciseDef, Exercise } from '../../core';
import { get } from '../../infrustructure/httpClient';
import { logPipe, logPipeProjection, tryExecute } from '../../util';

export type OptimalDbExerciseDef = { database: string } & BaseExerciseDef;
export type OptimalDbExerciseResult = string;

const Database = t.record(t.string, t.array(t.string));
type Database = t.TypeOf<typeof Database>;

const llm = new ChatOpenAI({
  modelName: 'gpt-4-1106-preview',
  //modelName: 'gpt-3.5-turbo-16k'
  temperature: 0,
  maxTokens: 4000,
  maxConcurrency: 3,
  maxRetries: 3,
});
const prompt = ChatPromptTemplate.fromMessages([
  new SystemMessage(`Skompresuj tekst użytkownika
Przekształć oryginalny tekst w listę faktów

### Instrukcje
- Pomiń imię
- Użyj równoważników zdań
- Bądź jak najbardziej zwięzły, opisz fakty jak najkrócej
- użyj maksymalnie 8 słów na fakt
- wymień WSZYSTKIE fakty z oryginalnego tekstu

### Przykłady
- Lubi kino
- Był na Malediwach
- Jest programistą
`),
  ['user', '{input}'],
]);

const llmChain = new LLMChain({ llm, prompt, outputKey: 'summary' });
export const optimaldb: Exercise<OptimalDbExerciseDef, OptimalDbExerciseResult> = {
  name: 'optimaldb',
  action: v => optimise(v.database),
};

const optimise = (databaseUrl: string): TaskEither<Error, string> =>
  pipe(getDatabase(databaseUrl), flatMap(compressKnowledge));

const getDatabase = (databaseUrl: string): TaskEither<Error, Database> =>
  get(databaseUrl, { responseValidator: Database.decode });

const compressKnowledge = (database: Database): TaskEither<Error, string> =>
  pipe(
    sequenceArray(
      pipe(
        Object.entries(database),
        A.map(([key, lines]) => [key, lines.join('\n')]),
        A.map(([name, text]) => summarise(name, text)),
      ),
    ),
    map(summaries => summaries.join('\n\n')),
    logPipeProjection(sizeInKBytes, '$Total Summary length'),
  );

const summarise = (name: string, text: string): TaskEither<Error, string> =>
  pipe(
    tryExecute(() => llmChain.call({ input: text })),
    map(({ summary }) => summary),
    map(summary => summaryTemplate(name, summary)),
    logPipe(`${name} summary`),
    logPipeProjection(sizeInKBytes, `${name} summary length`),
  );

const summaryTemplate = (name: string, summary: string) =>
  `#${name}
  ${summary}`;

const sizeInKBytes = (text: string): number => new Blob([text]).size / 1024;
