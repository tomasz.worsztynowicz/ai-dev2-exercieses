import * as E from 'fp-ts/Either';
import { pipe } from 'fp-ts/function';
import { flatMap, fromEither, left, map, of, TaskEither } from 'fp-ts/TaskEither';
import { LLMChain } from 'langchain/chains';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { ChatPromptTemplate } from 'langchain/prompts';
import { SystemMessage } from 'langchain/schema';
import { OpenAI } from 'openai';
import { Action, BaseExerciseDef, Exercise } from '../../core';
import { logPipe, tryExecute } from '../../util';

export type WhisperExerciseDef = { msg: string } & BaseExerciseDef;
export type WhisperExerciseResult = string;

const extractAutoFilePrompt = ChatPromptTemplate.fromMessages([
  new SystemMessage(`
Extract the URL of the audio file from text.
Based on file extension recognize the type of audio file.
Return JSON in format {"url": (URL from the text), "type": (the audio file type as a short abbreviation)}
Always skip any additional comments
text:`),
  ['human', '{msg}'],
]);

const punctuationSystemPrompt = ChatPromptTemplate.fromMessages([
  new SystemMessage(
    `
Your task is to correct any spelling discrepancies in the transcribed text below. Only add necessary punctuation such as periods, commas, and capitalization, and use only the context provided.

DON'T add quotation marks at the beginning and the end of the text
`,
  ),
  ['human', '{text}'],
]);

type AudioFileMetadata = { url: string; type: string };
const extractAudioFileMetadata = (msg: string): TaskEither<Error, AudioFileMetadata> =>
  pipe(
    of(new ChatOpenAI({ modelName: 'gpt-3.5-turbo', temperature: 0, maxTokens: 500 })),
    map(llm => new LLMChain({ llm, prompt: extractAutoFilePrompt })),
    flatMap(llmChain => tryExecute(() => llmChain.call({ msg }))),
    flatMap(({ text }) =>
      fromEither(E.tryCatch(() => JSON.parse(text) as AudioFileMetadata, E.toError)),
    ),
  );

const getAudioFileStream = (file: AudioFileMetadata): TaskEither<Error, Response> =>
  pipe(
    tryExecute(() => fetch(file.url)),
    flatMap((response: Response) =>
      response.ok ? of(response) : left(new Error('Failed to fetch')),
    ),
  );

const fixPunctuation = (text: string): TaskEither<Error, string> =>
  pipe(
    of(new ChatOpenAI({ modelName: 'gpt-4', temperature: 0, maxTokens: 500 })),
    map(llm => new LLMChain({ llm, prompt: punctuationSystemPrompt })),
    flatMap(llmChain => tryExecute(() => llmChain.call({ text }))),
    map(({ text }) => text),
  );

const transcriptAudioFile = (file: AudioFileMetadata): TaskEither<Error, string> =>
  pipe(
    getAudioFileStream(file),
    flatMap(stream =>
      tryExecute(() =>
        new OpenAI().audio.transcriptions.create({
          file: stream,
          model: 'whisper-1',
          response_format: 'json',
        }),
      ),
    ),
    map(({ text }) => text),
  );

const action: Action<WhisperExerciseDef, WhisperExerciseResult> = ({ msg }) =>
  pipe(
    extractAudioFileMetadata(msg),
    logPipe('Extract audio file metadata'),
    flatMap(file => transcriptAudioFile(file)),
    logPipe('Transcripted audio file'),
    flatMap(text => fixPunctuation(text)),
    logPipe('Fixed punctuation'),
  );

export const whisper: Exercise<WhisperExerciseDef, WhisperExerciseResult> = {
  name: 'whisper',
  action,
};
