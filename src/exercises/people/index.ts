import { file } from 'bun';
import { pipe } from 'fp-ts/function';
import { flatMap, map, sequenceSeqArray, TaskEither } from 'fp-ts/TaskEither';
import { LLMChain } from 'langchain/chains';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { Document } from 'langchain/document';
import { OpenAIEmbeddings } from 'langchain/embeddings/openai';
import { ChatPromptTemplate } from 'langchain/prompts';
import { is } from 'ramda';
import { v4 as uuidv4 } from 'uuid';
import { BaseExerciseDef, Exercise } from '../../core';
import { search, UpsertResult, upsertVectors, Vector } from '../../infrustructure/qdrant';
import { logPipe, logPipeProjection, logReplace, tap, tryExecute } from '../../util';

const FILE_PATH = import.meta.dir + '/people.json';
const COLLECTION_NAME = 'people';

const embeddings = new OpenAIEmbeddings({ maxConcurrency: 5, maxRetries: 5 });

export type PeopleExerciseDef = { question: string } & BaseExerciseDef;
export type PeopleExerciseResult = string;

type RawEntry = {
  imie: string;
  nazwisko: string;
  wiek: number;
  o_mnie: string;
  ulubiona_postac_z_kapitana_bomby: string;
  ulubiony_serial: string;
  ulubiony_film: string;
  ulubiony_kolor: string;
};

type TransformedEntry = {
  uuid: string;
  text: string;
};

const fixEncoding = (val: string | number): string | number =>
  is(String, val)
    ? val.replace(/\\u([0-9a-f]{4})/gi, (match, p1) => String.fromCharCode(parseInt(p1, 16)))
    : val;

const fixEntryEncoding = (entry: RawEntry): RawEntry =>
  Object.fromEntries(
    Object.entries(entry).map(([key, value]) => [key, fixEncoding(value)]),
  ) as RawEntry;

const getEntries = (filePath: string): TaskEither<Error, RawEntry[]> =>
  pipe(
    tryExecute(() => file(filePath).json<RawEntry[]>()),
    map(entries => entries.map(fixEntryEncoding)),
  );

const transformEntry = (entry: RawEntry): TransformedEntry => ({
  uuid: uuidv4(),
  text: `Nazywam się ${entry.imie} ${entry.nazwisko}. Mój ulubiony kolor to ${entry.ulubiony_kolor}, ${entry.o_mnie}`,
});
const mapToDocument = (entry: TransformedEntry): Document<TransformedEntry> => ({
  pageContent: entry.text,
  metadata: { ...entry },
});

const mapToVector = (
  document: Document<TransformedEntry>,
  index: number,
): TaskEither<Error, Vector<TransformedEntry>> =>
  pipe(
    tryExecute(() => embeddings.embedDocuments([document.pageContent])),
    map(([embedding]) => ({
      id: document.metadata.uuid,
      vector: embedding,
      payload: document.metadata,
    })),
    tap(() => logReplace(`Embedded ${index + 1} documents`)),
  );

export const buildIndex = (): TaskEither<Error, UpsertResult> =>
  pipe(
    getEntries(FILE_PATH),
    logPipeProjection(entries => `Read ${entries.length} entries`),
    map(entries => entries.map(transformEntry)),
    map(entries => entries.map(mapToDocument)),
    flatMap(documents => sequenceSeqArray(documents.map(mapToVector))),
    flatMap(upsertVectors(COLLECTION_NAME)),
    logPipe('Build Index result'),
  );

const getContext = (question: string): TaskEither<Error, string[]> =>
  pipe(
    tryExecute(() => embeddings.embedQuery(question)),
    flatMap(queryEmbeddings =>
      search<TransformedEntry>({
        collectionName: COLLECTION_NAME,
        vector: queryEmbeddings,
        limit: 1,
      }),
    ),
    logPipe('Query result'),
    map(results => results.map(v => v.payload.text)),
  );

const answerTheQuestion =
  (question: string) =>
  (context: string[]): TaskEither<Error, string> => {
    const llm = new ChatOpenAI({
      maxTokens: 500,
      maxRetries: 5,
      maxConcurrency: 5,
      modelName: 'gpt-3.5-turbo',
      temperature: 0.5,
    });
    const prompt = ChatPromptTemplate.fromMessages([
      [
        'system',
        `Answer the question using the context below and nothing else. Use the question language.
        context###
        {context}
        ###`,
      ],
      ['user', '{question}'],
    ]);
    const chainLLM = new LLMChain({ llm, prompt });
    return pipe(
      tryExecute(() => chainLLM.call({ question, context: context.join('\n') })),
      map(({ text }) => text),
      logPipe('Answer'),
    );
  };

export const people: Exercise<PeopleExerciseDef, PeopleExerciseResult> = {
  name: 'people',
  action: v => pipe(getContext(v.question), flatMap(answerTheQuestion(v.question))),
};
