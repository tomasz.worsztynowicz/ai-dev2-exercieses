import * as E from 'fp-ts/Either';
import { pipe } from 'fp-ts/function';
import { flatMap, fromEither, map, of, orElse, TaskEither } from 'fp-ts/TaskEither';
import { LLMChain } from 'langchain/chains';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { ChainValues } from 'langchain/dist/schema';
import { ChatPromptTemplate } from 'langchain/prompts';
import { HumanMessage, SystemMessage } from 'langchain/schema';
import { Action, BaseExerciseDef, Exercise } from '../../core';
import { getExerciseDef } from '../../infrustructure/aidev2client';
import { logPipe, tryExecute } from '../../util';

export type WhoAmIExerciseDef = { hint: string } & BaseExerciseDef;
export type WhoAmIExerciseResult = string;
type Guess = { solved: boolean; youAre?: string };

const systemMessage: SystemMessage = new SystemMessage(`
Let's play! Guess who I am!
I will be giving you hints and your task is to guess who I am based on my hints and your general knowledge.

Before guessing rethink twice if you know the right answer. Be sure of your answer, if not I will give the next hint.

Instructions:
- Answer shortly with JSON object and nothing else. 
- Do not add any comments or formatting.
- If you don't know who I am write: {"solved": false}
- If you know who I am write: {"solved": true, "youAre": (your guess)}
`);
const getHint = (): TaskEither<Error, string> =>
  pipe(
    getExerciseDef<WhoAmIExerciseDef>('whoami'),
    map(v => v.hint),
  );

const llm = new ChatOpenAI({ modelName: 'gpt-3.5-turbo', temperature: 0.5, maxTokens: 1000 });
const recoveryLLM = new ChatOpenAI({ modelName: 'gpt-4', temperature: 0.5, maxTokens: 1000 });

const guessWhoAmI = (
  hint: HumanMessage,
  previousGuesses: HumanMessage[],
): TaskEither<Error, string> => {
  const hints = [...previousGuesses, hint];
  return pipe(
    tryGuess(hints),
    flatMap(({ solved, youAre }) =>
      solved
        ? of(youAre!)
        : pipe(
            getHint(),
            flatMap(hint => guessWhoAmI(new HumanMessage(hint), hints)),
          ),
    ),
  );
};

const tryGuess = (hints: HumanMessage[]) =>
  pipe(
    tryWithLLM(llm, hints),
    logPipe('guess'),
    flatMap(parseGuessResponse),
    orElse(() =>
      pipe(tryWithLLM(recoveryLLM, hints), logPipe('recovery guess'), flatMap(parseGuessResponse)),
    ),
  );

const tryWithLLM = (llm: ChatOpenAI, hints: HumanMessage[]) =>
  tryExecute(() =>
    new LLMChain({
      llm,
      prompt: ChatPromptTemplate.fromMessages([systemMessage, ...hints]),
    }).call({}),
  );

const parseGuessResponse = ({ text }: ChainValues) =>
  fromEither(E.tryCatch(() => JSON.parse(text) as Guess, E.toError));

const action: Action<WhoAmIExerciseDef, WhoAmIExerciseResult> = ({ hint }) =>
  guessWhoAmI(new HumanMessage(hint), []);

export const whoami: Exercise<WhoAmIExerciseDef, WhoAmIExerciseResult> = {
  name: 'whoami',
  action,
};
