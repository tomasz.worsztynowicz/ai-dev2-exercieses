import { pipe } from 'fp-ts/function';
import { flatMap, of, TaskEither } from 'fp-ts/TaskEither';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { HumanMessage } from 'langchain/schema';
import { isNotNil } from 'ramda';
import { BaseExerciseDef, Exercise } from '../../core';
import { logPipe, tryExecute } from '../../util';
import { decode } from '../../util/decode.ts';
import { AICompletionMessage } from '../../util/functionCall.ts';
import {
  CountryPopulationArgs,
  countryPopulationSchema,
  getCountryPopulation,
} from './functions/countries.ts';
import { ExchangeRateArgs, exchangeRateSchema, getExchangeRate } from './functions/nbp.ts';

export type KnowledgeExerciseDef = { question: string } & BaseExerciseDef;
export type KnowledgeExerciseResult = unknown;

export const knowledge: Exercise<KnowledgeExerciseDef, KnowledgeExerciseResult> = {
  name: 'knowledge',
  action: v => pipe(selectAction(v.question), flatMap(invokeAction), logPipe('Answer')),
};

const llm = new ChatOpenAI({
  modelName: 'gpt-3.5-turbo-1106',
  maxTokens: 500,
  maxRetries: 5,
  temperature: 0,
}).bind({ functions: [exchangeRateSchema, countryPopulationSchema] });

const selectAction = (question: string): TaskEither<Error, AICompletionMessage> =>
  pipe(
    tryExecute(() => llm.invoke([new HumanMessage(question)])),
    flatMap(decode(AICompletionMessage.decode)),
    logPipe('Selected action'),
  );

const invokeAction = (action: AICompletionMessage): TaskEither<Error, string | number> => {
  const {
    content,
    additional_kwargs: { function_call },
  } = action;
  if (isNotNil(function_call)) {
    switch (function_call.name) {
      case exchangeRateSchema.name:
        return pipe(
          tryExecute(async () => JSON.parse(function_call.arguments)),
          flatMap(decode(ExchangeRateArgs.decode)),
          flatMap(({ currencyCode }) => getExchangeRate(currencyCode)),
        );
      case countryPopulationSchema.name:
        return pipe(
          tryExecute(async () => JSON.parse(function_call.arguments)),
          flatMap(decode(CountryPopulationArgs.decode)),
          flatMap(({ countryCode }) => getCountryPopulation(countryCode)),
        );
    }
  }
  return of(content || 'I do not know the right answer');
};
