import { pipe } from 'fp-ts/function';
import { map, TaskEither } from 'fp-ts/TaskEither';
import * as t from 'io-ts';
import { LLMChain } from 'langchain/chains';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { ChatPromptTemplate } from 'langchain/prompts';
import { SystemMessage } from 'langchain/schema';
import { ChatCompletionCreateParams } from 'openai/resources';
import { logPipe, tryExecute } from '../../../util';

const systemPrompt =
  'Answer precisely and concisely the user question based on your entire knowledge.';

const llm = new ChatOpenAI({
  modelName: 'gpt-3.5-turbo',
  maxTokens: 500,
  maxRetries: 5,
  temperature: 0.5,
});

const prompt = ChatPromptTemplate.fromMessages([
  new SystemMessage(systemPrompt),
  ['user', '{input}'],
]);

const llmChain = new LLMChain({ llm, prompt });

export const getGenericKnowledge = (question: string): TaskEither<Error, string> =>
  pipe(
    tryExecute(() => llmChain.call({ input: question })),
    map(({ text }) => text),
    logPipe('getGenericKnowledge result'),
  );

export const genericKnowledgeSchema: ChatCompletionCreateParams.Function = {
  name: 'getGenericKnowledge',
  description: 'Returns the answer for generic question based on the publicly available knowledge.',
  parameters: {
    type: 'object',
    properties: {
      question: {
        type: 'string',
        description:
          'The question to be answered e.g "Who was Aristotle?", "When Undated States was founded?"',
      },
    },
    required: ['question'],
  },
};

export const GenericKnowledgeArgs = t.type({ question: t.string });

export type GenericKnowledgeArgs = t.TypeOf<typeof GenericKnowledgeArgs>;
