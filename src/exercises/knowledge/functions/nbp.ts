import { pipe } from 'fp-ts/function';
import { flatMap, map, TaskEither } from 'fp-ts/TaskEither';
import * as t from 'io-ts';
import { nonEmptyArray } from 'io-ts-types';
import { ChatCompletionCreateParams } from 'openai/resources';
import { get } from '../../../infrustructure/httpClient';
import { logPipe } from '../../../util';
import { decode } from '../../../util/decode.ts';

const nbpUrl = 'http://api.nbp.pl/api/exchangerates/rates/a/';

const Rate = t.type({
  no: t.string,
  effectiveDate: t.string,
  mid: t.number,
});

const NbpExchangeRate = t.type({
  table: t.string,
  currency: t.string,
  code: t.string,
  rates: nonEmptyArray(Rate),
});

export type ExchangeRate = number;

export const getExchangeRate = (currencyCode: string): TaskEither<Error, ExchangeRate> =>
  pipe(
    get(nbpUrl + currencyCode),
    flatMap(decode(NbpExchangeRate.decode)),
    map(({ rates }) => rates[0].mid),
    logPipe('getExchangeRate result'),
  );

export const exchangeRateSchema: ChatCompletionCreateParams.Function = {
  name: 'getExchangeRate',
  description: 'Returns exchange rate for selected currency',
  parameters: {
    type: 'object',
    properties: {
      currencyCode: {
        type: 'string',
        description: 'The currency code in ISO format e.g. PLN, EUR, USD',
      },
    },
    required: ['currencyCode'],
  },
};

export const ExchangeRateArgs = t.type({ currencyCode: t.string });

export type ExchangeRateArgs = t.TypeOf<typeof ExchangeRateArgs>;
