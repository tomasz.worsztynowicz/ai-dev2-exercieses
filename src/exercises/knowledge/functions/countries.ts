import { pipe } from 'fp-ts/function';
import { flatMap, map, TaskEither } from 'fp-ts/TaskEither';
import * as t from 'io-ts';
import { nonEmptyArray } from 'io-ts-types';
import { ChatCompletionCreateParams } from 'openai/resources';
import { get } from '../../../infrustructure/httpClient';
import { logPipe } from '../../../util';
import { decode } from '../../../util/decode.ts';

const countriesUrl = ' https://restcountries.com/v3.1/alpha/';

const Country = t.type({
  population: t.number,
});

export type Population = number;

export const getCountryPopulation = (countryCode: string): TaskEither<Error, Population> =>
  pipe(
    get(countriesUrl + countryCode),
    flatMap(decode(nonEmptyArray(Country).decode)),
    map(([country]) => country.population),
    logPipe('getCountryPopulation result'),
  );

export const countryPopulationSchema: ChatCompletionCreateParams.Function = {
  name: 'getCountryPopulation',
  description: 'Returns population of the selected country',
  parameters: {
    type: 'object',
    properties: {
      countryCode: {
        type: 'string',
        description: 'The country code in ISO format e.g. PL, US, FI',
      },
    },
    required: ['countryCode'],
  },
};

export const CountryPopulationArgs = t.type({ countryCode: t.string });

export type CountryPopulationArgs = t.TypeOf<typeof CountryPopulationArgs>;
