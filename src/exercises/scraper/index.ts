import { orElse } from 'fp-ts/es6/TaskEither';
import { pipe } from 'fp-ts/function';
import { flatMap, left, map, TaskEither } from 'fp-ts/TaskEither';
import { LLMChain } from 'langchain/chains';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { ChatPromptTemplate } from 'langchain/prompts';
import { Action, BaseExerciseDef, Exercise } from '../../core';
import { logPipe, tryExecute } from '../../util';

export type ScraperExerciseDef = { input: string; question: string } & BaseExerciseDef;
export type ScraperExerciseResult = string;

const tryReadFile = (url: string, tries: number): TaskEither<Error, string> => {
  if (tries === 0) {
    console.error('All tries of reading file failed');
    return left(new Error('All tries of reading file failed'));
  }
  console.log('Trying to read file, attempts left: ', tries - 1);
  return pipe(
    fetchFile(url),
    flatMap(parseResponse),
    logPipe('file content'),
    orElse(() => tryReadFile(url, tries - 1)),
  );
};

const fetchFile = (url: string): TaskEither<Error, Response> =>
  tryExecute(() =>
    fetch(url, {
      headers: {
        'User-Agent':
          'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/118.0.0.0 Safari/537.36',
      },
    }),
  );

const parseResponse = (response: Response): TaskEither<Error, string> => {
  console.log(`status: ${response.status} ${response.statusText}`);
  return response.ok ? tryExecute(() => response.text()) : left(new Error('file reading error'));
};

const systemPrompt = `
Answer the questions as truthfully as possible using the context below and nothing else. If you don't know the answer, say, "I don't know.”
Answer briefly and concisely

context###
{text}
###
`;
const answerQuestion =
  (question: string) =>
  (text: string): TaskEither<Error, string> => {
    const llm = new ChatOpenAI({ modelName: 'gpt-3.5-turbo', temperature: 0.5, maxTokens: 500 });
    const prompt = ChatPromptTemplate.fromMessages([
      ['system', systemPrompt],
      ['user', '{question}'],
    ]);
    const chainLLM = new LLMChain({ llm, prompt });
    return pipe(
      tryExecute(() => chainLLM.call({ question, text })),
      map(({ text }) => text),
      logPipe('answer'),
    );
  };

const readFile = (url: string): TaskEither<Error, string> => tryReadFile(url, 10);
const action: Action<ScraperExerciseDef, ScraperExerciseResult> = ({ input, question }) =>
  pipe(readFile(input), flatMap(answerQuestion(question)));

export const scraper: Exercise<ScraperExerciseDef, ScraperExerciseResult> = {
  name: 'scraper',
  action,
};
