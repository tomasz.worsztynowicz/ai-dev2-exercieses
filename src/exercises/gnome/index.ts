import { pipe } from 'fp-ts/function';
import { flatMap, fromNullable, map, TaskEither } from 'fp-ts/TaskEither';
import { OpenAI } from 'openai';
import { ChatCompletion } from 'openai/resources';
import { BaseExerciseDef, Exercise } from '../../core';
import { logPipe, tryExecute } from '../../util';

export type GnomeExerciseDef = { url: string } & BaseExerciseDef;
export type GnomeExerciseResult = unknown;

export const gnome: Exercise<GnomeExerciseDef, GnomeExerciseResult> = {
  name: 'gnome',
  action: v => recognize(v.url),
};

const gnomeRecognitionInstructionMessage: OpenAI.Chat.ChatCompletionSystemMessageParam = {
  role: 'system',
  content: `If the image below presents a gnome with a hat on his head, write the color of his hat and nothing else. Use Polish names of colors.  
  Otherwise, answer "ERROR".`,
};

const gnomeImageMessage = (url: string): OpenAI.Chat.ChatCompletionUserMessageParam => ({
  role: 'user',
  content: [
    {
      type: 'image_url',
      image_url: { url },
    },
  ],
});

const gnomeRecognitionParams = (
  url: string,
): OpenAI.Chat.Completions.ChatCompletionCreateParams => ({
  model: 'gpt-4-vision-preview',
  messages: [gnomeRecognitionInstructionMessage, gnomeImageMessage(url)],
  max_tokens: 1000,
});

const recognize = (url: string): TaskEither<Error, string> =>
  pipe(
    tryExecute(() => new OpenAI().chat.completions.create(gnomeRecognitionParams(url))),
    map(completion => completion as ChatCompletion),
    flatMap(({ choices: [head, ..._] }) => fromNullable(new Error('No answer'))(head)),
    flatMap(({ message: { content } }) => fromNullable(new Error('No answer'))(content)),
    logPipe('Result'),
  );
