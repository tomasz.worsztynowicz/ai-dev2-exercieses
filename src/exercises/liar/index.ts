import { pipe } from 'fp-ts/function';
import { chain, map, of } from 'fp-ts/TaskEither';
import { LLMChain } from 'langchain/chains';
import { ChatOpenAI } from 'langchain/chat_models/openai';
import { ChatPromptTemplate } from 'langchain/prompts';
import { BaseExerciseDef, Exercise } from '../../core';
import { post } from '../../infrustructure/httpClient';
import { logPipe, tryExecute } from '../../util';

export type LairExerciseDef = { answer?: string } & BaseExerciseDef;
export type LairExerciseResult = string;
const aiDev2Url = process.env.AI_DEV2_EXERCISES_URL ?? '';

const getLlmChat = () => {
  const chatPrompt = ChatPromptTemplate.fromMessages([
    [
      'system',
      `
Take a deep breath and verify the accuracy of the answer to a question: '{question}'.
Consider all the pros and cons and try to understand the intention of the answer before the final judge.
Answer the question ultra-briefly with 'YES' or 'NO'.`,
    ],
    ['human', '{answer}'],
  ]);
  const chat = new ChatOpenAI({ temperature: 0, modelName: 'gpt-4', maxTokens: 500 });
  return new LLMChain({ prompt: chatPrompt, llm: chat });
};
const action =
  (question: string) =>
  ({ answer }: LairExerciseDef) =>
    pipe(
      of(getLlmChat()),
      chain(llmChain => tryExecute(() => llmChain.call({ question, answer }))),
      map(({ text }) => text),
      logPipe(`Q: ${question} A: ${answer} Judge`),
    );

export const lair = (question: string): Exercise<LairExerciseDef, LairExerciseResult> => ({
  name: 'liar',
  action: action(question),

  getExerciseDefinitionModifier: token => {
    const formData = new FormData();
    formData.append('question', question);
    return post(`${aiDev2Url}/task/${token}`, formData, { payloadToFormData: true });
  },
});
