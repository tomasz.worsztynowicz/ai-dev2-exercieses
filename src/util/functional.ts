import { pipe } from 'fp-ts/function';
import { map, TaskEither, tryCatch } from 'fp-ts/TaskEither';

export const tryExecute = <E, T>(lazy: () => Promise<T>): TaskEither<Error, T> =>
  tryCatch(lazy, err => err as Error);

export const tap =
  <E, T>(f: (t: T) => void) =>
  (te: TaskEither<E, T>): TaskEither<E, T> =>
    pipe(
      te,
      map(t => {
        f(t);
        return t;
      }),
    );
