import * as E from 'fp-ts/Either';
import { pipe } from 'fp-ts/function';
import { fromEither, TaskEither } from 'fp-ts/TaskEither';
import { Validation } from 'io-ts';

export const decode =
  <T>(validator: (val: unknown) => Validation<T>) =>
  (value: unknown): TaskEither<Error, T> =>
    pipe(
      validator(value),
      E.mapLeft(errors => JSON.stringify(errors)),
      E.mapLeft(errMsg => new Error('unrecognized data:' + errMsg)),
      fromEither,
    );
