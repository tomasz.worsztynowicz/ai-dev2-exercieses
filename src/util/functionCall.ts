import * as t from 'io-ts';

export const FunctionCall = t.type({
  name: t.string,
  arguments: t.string,
});

export const AdditionalKwargs = t.partial({
  function_call: FunctionCall,
});

export const AICompletionMessage = t.type({
  content: t.union([t.string, t.null]),
  additional_kwargs: AdditionalKwargs,
});

export type AICompletionMessage = t.TypeOf<typeof AICompletionMessage>;
