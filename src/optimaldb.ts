import { execute } from './core';
import { optimaldb } from './exercises/optimaldb';

await execute(optimaldb);
