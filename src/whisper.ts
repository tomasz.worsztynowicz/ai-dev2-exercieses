import { execute } from './core';
import { whisper } from './exercises/whisper';

await execute(whisper);
