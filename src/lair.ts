import { pipe } from 'fp-ts/function';
import { chain, map, sequenceSeqArray } from 'fp-ts/TaskEither';
import { LLMChain } from 'langchain/chains';
import { OpenAI } from 'langchain/llms/openai';
import { PromptTemplate } from 'langchain/prompts';
import { lair } from './exercises/liar';
import { runExercise } from './infrustructure/aidev2client';
import { logPipe, tryExecute } from './util';

const model = new OpenAI({ temperature: 0.5, modelName: 'gpt-3.5-turbo' });
const prompt = PromptTemplate.fromTemplate(
  'Generate {n} questions for a quiz about nature and the environment. \n' +
    'As a response generate JSON array where each question is one array element. Do not generate any other text',
);
const llmChain = new LLMChain({ llm: model, prompt });

await pipe(
  tryExecute(() => llmChain.call({ n: '5' })),
  map(({ text }) => JSON.parse(text) as string[]),
  logPipe('Generated questions'),
  chain(questions => sequenceSeqArray(questions.map(q => runExercise(lair(q))))),
)();
